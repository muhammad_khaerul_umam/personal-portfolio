import { PictureAsPdf } from "@material-ui/icons";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import React from "react";
import "./ButtonResume.css";

const useStyles = makeStyles((theme) => ({
  button: {
    margin: theme.spacing(1),
  },
}));

function ButtonResume() {
  const classes = useStyles();

  return (
    <div className="button_resume1">
      <Button
        href="https://drive.google.com/file/d/1z7I2n9sxjk2WV5xTFzNVGiAA-5GZ_Hi1/view?usp=sharing"
        target="blank"
        variant="contained"
        color="secondary"
        className={classes.button}
        startIcon={<PictureAsPdf />}
        size="large"
      >
        {" "}
        See Resume
      </Button>
      {/* <ButtonIcon
        href="https://drive.google.com/file/d/1z7I2n9sxjk2WV5xTFzNVGiAA-5GZ_Hi1/view?usp=sharing"
        target="blank"
      >
        See Resume
        <PictureAsPdf></PictureAsPdf>
      </ButtonIcon> */}
    </div>
  );
}

export default ButtonResume;
