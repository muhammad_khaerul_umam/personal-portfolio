// import React, { useState } from "react";
import React from "react";
import { Button } from "@material-ui/core";
import "./Banner.css";
import { useHistory } from "react-router";
import ButtonResume from "./ButtonResume";
// import { Spring } from "react-spring";

function Banner() {
  const history = useHistory();

  return (
    <div className="banner">
      <div className="banner__info">
        <h3>Hi, my name is </h3>
        <h1>Muhammad </h1>
        <h2>Khaerul Umam. </h2>

        <div className="quotes">
          <i class="fas fa-quote-left"></i>
          <h3>Learn everything</h3>
          {"teach anything and growup "}
          <i class="fas fa-quote-right"></i>
        </div>

        <div className="button__resume">
          <Button variant="outlined" onClick={() => history.push("/search")}>
            Contact Me
          </Button>
        </div>
      </div>

      <div className="skills">
        <h1>Skills Require.</h1>
        <h2>Web Development__</h2>
        <div className="iconSkills">
          <div className="iconHTML5">
            <i className="fab fa-html5 fa-3x"></i> <p>⚡html-5</p>
          </div>
          <div className="iconREACT">
            <i className="fab fa-react fa-3x"></i> <p>⚡reactJS</p>
          </div>
          <div className="iconNPM">
            <i className="fab fa-npm fa-3x"></i> <p>⚡npm</p>
          </div>
          <div className="iconNode">
            <i className="fab fa-node fa-3x"></i> <p>⚡nodeJS</p>
          </div>
          <div className="iconFire">
            <i className="fas fa-fire fa-3x"></i> <p>⚡firebase</p>
          </div>
          <div className="iconSQL">
            <i className="fas fa-database fa-3x"></i> <p>⚡sql-db</p>
          </div>
          <div className="iconCSS">
            <i className="fab fa-css3-alt fa-3x"></i> <p>⚡css3</p>
          </div>
          <div className="iconGIT">
            <i className="fab fa-git-alt fa-3x"></i> <p>⚡git</p>
          </div>
          <div className="iconW">
            <i className="fab fa-wordpress fa-3x"></i> <p>⚡wordpress</p>
          </div>
        </div>

        <div className="skillsOther">
          <div className="iconSkills2">
            <h2>Telecomunications__ </h2>

            <ul>
              <li>⚡ Genex-Probe | Assistance</li>
            </ul>

            <ul>
              <li>⚡ TEMS Investigation</li>
            </ul>

            <ul>
              <li>⚡ Nemo Outdoor/Indoor</li>
            </ul>

            <ul>
              <li>⚡ MapInfo Proffesional</li>
            </ul>

            <ul>
              <li>⚡ Fundamental Fiber Optic</li>
            </ul>

            <ul>
              <li>⚡ CISCO Devices </li>
            </ul>
          </div>

          <div className="iconSkills3">
            <h2>Desain and Supports__ </h2>

            <ul>
              <li>⚡ Adobe Illustrator</li>
            </ul>

            <ul>
              <li>⚡ Microsoft Office</li>
            </ul>
          </div>
        </div>
        <ButtonResume />
      </div>
    </div>
  );
}

export default Banner;
