import React from "react";
import { GitHub, Instagram, Twitter, Mail, LinkedIn } from "@material-ui/icons";
import "./Header.css";
import Menubar from "./MenuBar";

function Header() {
  return (
    <div className="header">
      <div className="header__right">
        <a href="https://twitter.com/mkhmm3?s=08" target="blank">
          <Twitter />
        </a>

        <a href="https://github.com/khaerulmuhammad" target="blank">
          <GitHub />
        </a>

        <a href="https://www.instagram.com/khaerul_ryle/" target="blank">
          <Instagram />
        </a>

        <a href="mailto:khaerulmuhammad1994@gmail.com" target="blank">
          <Mail />
        </a>

        <a
          href="https://www.linkedin.com/in/muhammad-khaerul-umam-856445200/"
          target="blank"
        >
          <LinkedIn />
        </a>
      </div>
      <Menubar />
    </div>
  );
}

export default Header;
