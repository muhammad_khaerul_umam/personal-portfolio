import React from "react";
import "./Footer.css";
import { Link } from "react-router-dom";
import LogoUmam from "./images/logoumam.png";
import { Email } from "@material-ui/icons";

function Footer() {
  return (
    <div className="footer">
      <Link to="/" target="blank">
        <img className="footer__icon" src={LogoUmam} alt="logo umam" />
      </Link>
      <p>
        © 2020 Portfolio | Muhammad Khaerul Umam | Tegal, Central Java -
        Indonesia - 52463
      </p>

      <h4>
        <Email /> | khaerulmuhammad1994@gmail.com
      </h4>
    </div>
  );
}

export default Footer;
