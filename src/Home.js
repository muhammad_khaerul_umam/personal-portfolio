import React from "react";
import "./Home.css";
import Banner from "./Banner";
import Card from "./Card";
import Experience from "./Experience";
import Education from "./Education";
import FakeDiscord from "../src/images/fake-discord.png";
import PersonalPf from "../src/images/logo-gradien.png";
import GameOne from "../src/images/minigame.png";
import WaClone from "../src/images/wa.png";
import Card2 from "./Card2";
import Card3 from "./Card3";
import Card4 from "./Card4";

function Home() {
  return (
    <div className="home">
      <div className="banner__home">
        <Banner />
      </div>
      <div className="myProjects">
        <h1>My Projects ...</h1>
        <p>
          ~ I build several website applications for study and business purpose
          ~
        </p>
      </div>
      <div id="project" className="home__section">
        <Card
          className="card__"
          src={FakeDiscord}
          alt="foto fakediscord"
          title="Fake Discord"
          description="Realtime Chatting Built using ReactJS for the purpose of learning and exploring React Components.. Integrated data with firebase.  Responsive UI. "
        />

        <Card2
          className="card__"
          src={PersonalPf}
          title="Personal | Portfolio"
          description="Built using ReactJS for the purpose of learning and exploring React Components. Integrated data with firebase. Responsive UI."
        />

        <Card3
          className="card__"
          src={WaClone}
          title="WhatsApp Clone"
          description="Built using ReactJS for the purpose of learning and exploring React Components and React Hooks with responsive UI. "
        />
      </div>

      <div id="project" className="home__section">
        <Card4
          className="card__"
          src={GameOne}
          title="Team|One GamePlay"
          description="Built using ReactJS for the purpose of learning. Login with Google Account from the features that exist in Firebase authentication"
        />

        <Card
          className="card__"
          src="https://media.istockphoto.com/videos/animated-lettering-coming-soon-in-speech-bubble-video-id1147665152?s=640x640"
          title="UNDER DEVELOPMENT"
          description="Under development under development under development  under developmentunder development under development under development  "
        />
        <Card
          className="card__"
          src="https://media.istockphoto.com/videos/animated-lettering-coming-soon-in-speech-bubble-video-id1147665152?s=640x640"
          title="UNDER DEVELOPMENT"
          description=" Under development under development under development under development under developmentunder development under development"
        />
      </div>
      <div id="education__umam" className="myExperience">
        <h1>Educations and Certifications.</h1>
        <p>
          ~ I graduated from college and attended several non-formal education
          to deepen my skills ~
        </p>
      </div>
      <Experience />
      <div id="experiencee" className="myExperience">
        <h1>Experience ...</h1>
        <p>
          ~ I have some work experience as a freelancer and engineer at a
          company ~
        </p>
      </div>
      <Education />
    </div>
  );
}

export default Home;
