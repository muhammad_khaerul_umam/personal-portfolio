import { Button } from "@material-ui/core";
import { WhatsApp } from "@material-ui/icons";
import React, { useState } from "react";
import { db } from "./Firebase";
import "./SearchPage.css";
import { makeStyles } from "@material-ui/core/styles";
import Alert from "@material-ui/lab/Alert";
import IconButton from "@material-ui/core/IconButton";
import Collapse from "@material-ui/core/Collapse";
import CloseIcon from "@material-ui/icons/Close";
import { useSpring, animated } from "react-spring";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
    color: "green",
  },
}));

function SearchPage() {
  const props = useSpring({ opacity: 1, from: { opacity: -100 } });

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();

    db.collection("contacts")
      .add({
        name: name,
        email: email,
        message: message,
      })

      .catch((error) => {
        alert(error.message);
      });

    setName("");
    setEmail("");
    setMessage("");
  };

  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  return (
    <animated.div style={props}>
      <form className="formInput" onSubmit={handleSubmit}>
        <h1>Contact Me 🚀</h1>

        <h2>
          "I am available on almost every social media. You can message me, I
          will reply within 24 hours "
        </h2>

        <label> Name :</label>
        <input
          placeholder="Name"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />

        <label> Email :</label>
        <input
          placeholder="Email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />

        <label> Message :</label>
        <textarea
          placeholder="Message"
          value={message}
          onChange={(e) => setMessage(e.target.value)}
        ></textarea>

        <Button
          type="submit"
          onClick={() => {
            setOpen(true);
          }}
        >
          Submit
        </Button>
        <Collapse in={open}>
          <Alert
            action={
              <IconButton
                aria-label="close"
                color="inherit"
                size="small"
                onClick={() => {
                  setOpen(false);
                }}
              >
                <CloseIcon fontSize="inherit" />
              </IconButton>
            }
          >
            Thanks your message has been sent!
          </Alert>
        </Collapse>
        <h4>
          <WhatsApp /> {"Telephone / WhatsApp"} +62 8953-6541-8682
        </h4>
      </form>
    </animated.div>
  );
}

export default SearchPage;
