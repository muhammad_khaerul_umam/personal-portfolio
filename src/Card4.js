import { GitHub, Launch } from "@material-ui/icons";
import React from "react";

import "./Card4.css";

function Card4({ src, title, description, forward }) {
  return (
    <div className="card">
      <img src={src} alt="" />
      <div className="card__info">
        <h2>{title}</h2>
        <h4>{description}</h4>

        <a
          href="https://gitlab.com/muhammad_khaerul_umam/challenge-fsw5-team-one"
          target="blank"
        >
          {forward}
          <GitHub></GitHub>
        </a>

        <a href=" https://fsw5-tim1-staging.herokuapp.com/" target="blank">
          <Launch>{forward}</Launch>
        </a>
      </div>
    </div>
  );
}

export default Card4;
