import firebase from "firebase";

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyA29SCxWd7DsxhB_TZDZk3PCdXCzW1XS7A",
  authDomain: "portfolio-umam-contact.firebaseapp.com",
  projectId: "portfolio-umam-contact",
  storageBucket: "portfolio-umam-contact.appspot.com",
  messagingSenderId: "512067486850",
  appId: "1:512067486850:web:f2e79250f71f15d943e09b",
  measurementId: "G-69HPZGKYNS",
});
const db = firebaseApp.firestore();
export { db };
