import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: "none",
    display: "flex",
    height: 304,
    color: "#ccd6f6",
    marginTop: "50px",
    marginRight: "50px",
    marginBottom: "100px",
    marginLeft: "50px",
  },
  tabs: {
    borderRight: `2px solid ${theme.palette.divider}`,
  },
}));

export default function Education() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <Tabs
        orientation="vertical"
        variant="scrollable"
        value={value}
        onChange={handleChange}
        aria-label="Vertical tabs example"
        className={classes.tabs}
      >
        <Tab label=" 2017 TO 2018 " {...a11yProps(0)} />
        <Tab label=" 2018 TO 2020" {...a11yProps(1)} />
        <Tab label=" 2020 - Present" {...a11yProps(2)} />
        <Tab label="Not Yet" {...a11yProps(3)} />
        <Tab label="Not Yet" {...a11yProps(4)} />
        <Tab label="Not Yet" {...a11yProps(5)} />
        <Tab label="Not Yet" {...a11yProps(6)} />
      </Tabs>
      <TabPanel value={value} index={0}>
        <h1>PT. Sinergi Aitikom as Engineer Tester</h1>
        <h3>
          Tools : MapInfo Profesional, Genex-Probe , Nemo Outdoor,
          TEMS-Investigation, MS-Excel.
        </h3>
        <br />
        <p>
          Record data in the field using tools, report any bad events from the
          cluster path that has been created. Serve complaints from VIP
          Customers to further report and optimize.
        </p>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <h1>PT. Sinergi Aitikom as Engineer Reporter</h1>
        <h3>
          Tools : MapInfo Profesional, Genex Assistant, MS-PowerPoint, MS-Excel.
        </h3>
        <br />
        <p>
          Performing technical supervision on-site, processing recorded data
          taken by engineers tester to be presented to the contractor from
          activities while on site.
        </p>
      </TabPanel>
      <TabPanel value={value} index={2}>
        <h1>Freelance Web Developer</h1>
        <h3>
          Tools : Adobe Illustrator, Visual Studio Code, ReactJS, Firebase,
          Material UI , ETC.
        </h3>
        <br />
        <p>
          I am a freelancer to create a website such as: company profile,
          portfolio, survey, and others. I'm also a vector graphic designer for
          creating a brand logo, poster, T-shirt design.
        </p>
      </TabPanel>
      <TabPanel value={value} index={3}>
        Not Yet
      </TabPanel>
      <TabPanel value={value} index={4}>
        Not Yet
      </TabPanel>
      <TabPanel value={value} index={5}>
        Not Yet
      </TabPanel>
      <TabPanel value={value} index={6}>
        Not Yet
      </TabPanel>
    </div>
  );
}
