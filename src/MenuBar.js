import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

const useStyles = makeStyles({
  Tabs: {
    flexGrow: 1,
    color: "#39b999",
    backgroundColor: "#050D16",
    borderRadius: "5px",
    // textDecoration: "bold",
  },
  Tab: {
    fontSize: "15px",
    fontWeight: "800",
    width: "17vw",
  },
});

export default function CenteredTabs() {
  //   const classes = menus ();
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Paper className={classes.Tabs}>
      <Tabs
        value={value}
        onChange={handleChange}
        indicatorColor="secondary"
        textColor="#39b999"
        centered
      >
        <Tab className={classes.Tab} label="Home" href="/" />
        <Tab className={classes.Tab} label=" Projects" href="#project" />
        <Tab
          className={classes.Tab}
          label="Education"
          href="#education__umam"
        />
        <Tab className={classes.Tab} label="Experience" href="#experiencee" />
      </Tabs>
    </Paper>
  );
}
