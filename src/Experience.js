import React from "react";
import "./Experience.css";

function Experience() {
  return (
    <div id="education-umam">
      <div class="wrapper">
        <div class="center-line">
          <a href="#" class="scroll-icon">
            <i class="fas fa-caret-up"></i>
          </a>
        </div>
        <div class="row row-1">
          <section>
            <i class="icon "></i>
            <div class="details">
              <span class="title">Universitas Bina Sarana Informatika</span>
              <span>2016</span>
            </div>
            <p>
              ⚡ I have studied basic Fullstack Web Developer subjects like
              HTML, Javascript, Algorithms, DBMS, etc. ⚡ Apart from this, I
              have done courses on NetCampus Academy (Cisco) and Full Stack Web
              Development on Binar Academy.
            </p>
            <div class="bottom">
              <a href="http://www.bsi.ac.id" target="blank">
                Visit Website
              </a>
              <i>-Yogyakarta</i>
            </div>
          </section>
        </div>
        <div class="row row-2">
          <section>
            <i class="icon"></i>
            <div class="details">
              <span class="title">NetCampus</span>
              <span> 2020</span>
            </div>
            <p>Cisco Lab Routing / Switching & Fiber Optic</p>
            <div class="bottom">
              <a
                href="https://drive.google.com/file/d/1WfINumjDxBt8gPVZCUeTSM6tF7YhddpU/view?usp=sharing"
                target="blank"
              >
                Certificate
              </a>
              <i>-Jakarta</i>
            </div>
          </section>
        </div>
        <div class="row row-1">
          <section>
            <i class="icon"></i>
            <div class="details">
              <span class="title">Binar Academy</span>
              <span>2021</span>
            </div>
            <p>
              Fullstack Web Development Builds Websites FrontEnd & BackEnd with
              HTML CSS3 Javascript and with framework supports
            </p>
            <div class="bottom">
              <a href="https://binaracademy.com" target="blank">
                Visit Website
              </a>
              <i>-Yogyakarta</i>
            </div>
          </section>
        </div>
        <div class="row row-2">
          <section>
            <i class="icon"></i>
            <div class="details">
              <span class="title">PT. Sinergi Aitikom</span>
              <span> 2020</span>
            </div>
            <p>Engineer Evaluation Test</p>
            <div class="bottom">
              <a
                href="https://drive.google.com/file/d/1WEuSa2NLwiHAsKbeeWXcXMWOuXAUHe3P/view?usp=sharing"
                target="blank"
              >
                Certificate
              </a>
              <i>-Jakarta</i>
            </div>
          </section>
        </div>

        <div class="row row-1">
          <section>
            <i class="icon"></i>
            <div class="details">
              <span class="title">LP3I</span>
              <span>2013</span>
            </div>
            <p>Microsoft Office Certification</p>
            <div class="bottom">
              <a
                href="https://drive.google.com/file/d/1RmDwcPe0AYlL6XLW3_9LWWVlAOgjEa-x/view?usp=sharing"
                target="blank"
              >
                Certificate
              </a>
              <i>-Tegal</i>
            </div>
          </section>
        </div>
      </div>
    </div>
  );
}

export default Experience;
