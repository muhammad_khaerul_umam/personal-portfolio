import { GitHub, Launch } from "@material-ui/icons";
import React from "react";

import "./Card.css";

function Card({ src, title, description, forward }) {
  return (
    <div className="card">
      <img src={src} alt="" />
      <div className="card__info">
        <h2>{title}</h2>
        <h4>{description}</h4>

        <a
          href="https://gitlab.com/muhammad_khaerul_umam/fake-discord"
          target="blank"
        >
          {forward}
          <GitHub></GitHub>
        </a>

        <a href="https://fake-discord-aa03a.web.app/" target="blank">
          <Launch>{forward}</Launch>
        </a>
      </div>
    </div>
  );
}

export default Card;
